<p>
	 &nbsp; Оплата возможна только банковской картой на сайте.
</p>
<p>
</p>
<p>
	 При оплате на сайте ru.boggi.com Вы будете автоматически перенаправлены на платежную форму процессингового центра PayU, для внесения данных Вашей банковской карты.<br>
	 Все данные, введенные Вами на платежной форме процессингового центра PayU, полностью защищены в соответствии с требованиями стандарта безопасности PCI DSS.
</p>
<p>
	 Мы получаем информацию только о совершенном Вами платеже.<br>
	 На указанный Вами при оформлении платежа адрес электронной почты, будет отправлено сообщение об авторизации платежа.<br>
	 Сразу после совершения платежа вы будете перенаправлены обратно на сайт ru.boggi.com
</p>
 <br>