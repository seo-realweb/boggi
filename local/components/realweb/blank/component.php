<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();


$bReturn = $arParams['RETURN'] == 'Y';

if ($bReturn) {
    ob_start();
    $this->IncludeComponentTemplate();
    $strContent = ob_get_contents();
    ob_end_clean();
    return $strContent;
} else {
    $this->IncludeComponentTemplate();
}
?>