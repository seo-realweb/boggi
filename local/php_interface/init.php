<?php

CModule::AddAutoloadClasses(
    '',
    array(
        'Realweb\Site' => '/local/php_interface/classes/Realweb/Site.php',
        'Realweb\Webp' => '/local/php_interface/classes/Realweb/Webp.php'
    )
);

\Bitrix\Main\EventManager::getInstance()->addEventHandler("main", "OnEndBufferContent", array('\Realweb\Webp', 'convertAllToWebp'));
