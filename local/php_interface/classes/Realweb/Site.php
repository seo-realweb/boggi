<?php
namespace Realweb;

use \Bitrix\Iblock\ElementTable;
use Bitrix\Main\HttpRequest;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Data\Cache;
use Realweb\BaseInclude\BaseIncludeTable;

class Site
{
    const DEFAULT_CACHE_TIME = 3600;

    public static function definders()
    {
        Loader::includeModule('iblock');
        $rsResult = \Bitrix\Iblock\IblockTable::getList([
            'select' => ['ID', 'IBLOCK_TYPE_ID', 'CODE'],
        ]);
        while ($row = $rsResult->fetch()) {
            $CONSTANT = ToUpper(implode('_', ['IBLOCK', $row['IBLOCK_TYPE_ID'], $row['CODE']]));
            if (!defined($CONSTANT)) {
                define($CONSTANT, $row['ID']);
            }
        }
    }

    public static function localRedirect301($url)
    {
        LocalRedirect($url, false, "301 Moved Permanently");
        exit();
    }

    public static function isMainPage()
    {
        global $APPLICATION;

        return $APPLICATION->GetCurPage(false) == SITE_DIR;
    }

    public static function normalizePhone($phone)
    {
        return preg_replace('/[^0-9]/', "", $phone);
    }

    public static function getDomain()
    {
        return (HttpRequest::isHttps() ? 'https' : 'http') . "://" . $_SERVER['SERVER_NAME'];
    }

    public static function getIBlockElements($filter, $navParams = false)
    {
        $arResult = [];
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = self::DEFAULT_CACHE_TIME;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($filter) . "|" . serialize($navParams));
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockElement::GetList(
                    ["SORT" => "ASC"],
                    $filter,
                    false,
                    $navParams,
                    ["*", "PROPERTY_*"]
                );
                while ($obElement = $rsElement->GetNextElement()) {
                    $arFields = $obElement->GetFields();
                    $arFields['FIELDS'] = $arFields;
                    if ($arFields['PREVIEW_PICTURE']) {
                        $arFields['PREVIEW_PICTURE'] = \CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
                    }
                    if ($arFields['DETAIL_PICTURE']) {
                        $arFields['DETAIL_PICTURE'] = \CFile::GetFileArray($arFields['DETAIL_PICTURE']);
                    }
                    $arFields['PROPERTIES'] = $obElement->GetProperties();
                    foreach ($arFields['PROPERTIES'] as &$prop) {
                        if ($prop['PROPERTY_TYPE'] == 'F') {
                            if (is_array($prop['VALUE'])) {
                                foreach ($prop['VALUE'] as $val) {
                                    $prop['DISPLAY_VALUE'][] = \CFile::GetFileArray($val);
                                }
                            } else {
                                $prop['DISPLAY_VALUE'] = \CFile::GetFileArray($prop['VALUE']);
                            }
                        } elseif ($prop['PROPERTY_TYPE'] == 'E' && intval($prop['VALUE']) > 0) {
                            $prop['ELEMENT'] = ElementTable::query()
                                ->setSelect(['NAME', 'ID'])
                                ->where('ID', '=', $prop['VALUE'])
                                ->setLimit(1)
                                ->exec()
                                ->fetch();
                        }
                        $arFields['PROPS'][$prop['CODE']] = $prop['VALUE'];
                    }
                    $arResult[$arFields['ID']] = $arFields;
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $filter['IBLOCK_ID']);
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }

        return $arResult;
    }

    public static function getSections($filter)
    {
        $arResult = [];
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = self::DEFAULT_CACHE_TIME;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . serialize($filter));
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arResult = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockSection::GetList(
                    ["SORT" => "ASC"],
                    $filter,
                    false,
                    ["*", "UF_*"]
                );
                while ($arSection = $rsElement->GetNext()) {
                    if ($arSection['PICTURE']) {
                        $arSection['PICTURE'] = \CFile::GetFileArray($arSection['PICTURE']);
                    }
                    $arResult[$arSection['ID']] = $arSection;
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . $filter['IBLOCK_ID']);
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arResult);
            }
        }

        return $arResult;
    }

    public static function setMetaElement($code)
    {
        global $APPLICATION;
        $arMeta = [];
        if (Loader::includeModule("iblock")) {
            global $CACHE_MANAGER;
            $cacheTime = self::DEFAULT_CACHE_TIME;
            $cacheDir = "/" . __FUNCTION__;
            $cacheId = md5(__FUNCTION__ . "|" . $code);
            $cache = Cache::createInstance();
            if ($cache->initCache($cacheTime, $cacheId, $cacheDir)) {
                $arMeta = $cache->GetVars();
            } elseif ($cache->StartDataCache()) {
                $CACHE_MANAGER->StartTagCache($cacheDir);
                $rsElement = \CIBlockElement::GetList(
                    ["SORT" => "ASC"],
                    ['IBLOCK_ID' => IBLOCK_CONTENT_CONTENT, 'CODE' => $code],
                    false,
                    ["ID", "IBLOCK_ID", "PROPERTY_META_CANONICAL"]
                );
                if ($arElement = $rsElement->GetNext()) {
                    if ($arElement['PROPERTY_META_CANONICAL_VALUE']) {
                        $arMeta["META"]["canonical"] = $arElement['PROPERTY_META_CANONICAL_VALUE'];
                    }
                    $obIpropValues = new \Bitrix\Iblock\InheritedProperty\ElementValues(IBLOCK_CONTENT_CONTENT,
                        $arElement['ID']);
                    $ipropValues = $obIpropValues->getValues();
                    if ($ipropValues['ELEMENT_PAGE_TITLE']) {
                        $arMeta["TITLE"] = $ipropValues['ELEMENT_PAGE_TITLE'];
                    }
                    if ($ipropValues['ELEMENT_META_TITLE']) {
                        $arMeta["META"]["title"] = $ipropValues['ELEMENT_META_TITLE'];
                    }
                    if ($ipropValues['ELEMENT_META_KEYWORDS']) {
                        $arMeta["META"]["keywords"] = $ipropValues['ELEMENT_META_KEYWORDS'];
                    }
                    if ($ipropValues['ELEMENT_META_DESCRIPTION']) {
                        $arMeta["META"]["description"] = $ipropValues['ELEMENT_META_DESCRIPTION'];
                    }
                }
                $CACHE_MANAGER->RegisterTag("iblock_id_" . IBLOCK_CONTENT_CONTENT);
                $CACHE_MANAGER->EndTagCache();
                $cache->endDataCache($arMeta);
            }
        }

        if ($arMeta["TITLE"]) {
            $APPLICATION->SetTitle($arMeta["TITLE"]);
        }
        foreach ($arMeta['META'] as $key => $value) {
            $APPLICATION->SetPageProperty($key, $value);
        }
    }

    public static function sendRequest($url, $params = [])
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_HEADER, false);
        curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
        curl_setopt($c, CURLOPT_URL, $url);
        $contents = curl_exec($c);
        curl_close($c);

        return $contents;
    }

    public static function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;
        if ($n > 10 && $n < 20) return $f5;
        $n = $n % 10;
        if ($n > 1 && $n < 5) return $f2;
        if ($n == 1) return $f1;
        return $f5;
    }

    public static function getCurPage()
    {
        global $APPLICATION;

        return $APPLICATION->GetCurPage(false);
    }

    public static function isCheckGooglePageSpeed()
    {
        return stripos($_SERVER['HTTP_USER_AGENT'], 'Chrome-Lighthouse') !== false;
    }

    public static function showIncludeText($code, $isHideChange = false)
    {
        global $APPLICATION;
        $APPLICATION->IncludeComponent(
            "realweb:base.include",
            ".default",
            array(
                "CODE" => $code,
                "COMPONENT_TEMPLATE" => ".default",
                "EDIT_TEMPLATE" => ""
            ),
            false,
            array(
                "SHOW_ICON" => $isHideChange ? "Y" : 'N',
            )
        );
    }

    public static function getIncludeTextByCode($strCode)
    {
        if (Loader::includeModule('realweb.baseinclude')) {
            $arItem = BaseIncludeTable::query()
                ->setSelect(array('TEXT'))
                ->where('CODE', '=', $strCode)
                ->exec()
                ->fetch();
            if ($arItem) {
                return $arItem['TEXT'];
            }
        }
    }
}
