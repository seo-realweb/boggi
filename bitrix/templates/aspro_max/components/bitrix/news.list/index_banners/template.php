<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die(); ?>
<? $this->setFrameMode(true); ?>

<? if ($arResult['ITEMS']): ?>
    <? foreach ($arResult['ITEMS'] as $i => $arItem): ?>
        <?
        // edit/add/delete buttons for edit mode
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        $count = $arItem['PROPERTIES']['BANNER_TYPE']['VALUE'] - 1;
        ?>
        <div id="<?= $this->GetEditAreaId($arItem['ID']) ?>" class="container index_banners">
            <div class="">
                <?
                if (isset($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"]) && is_array($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"])) {
                    foreach ($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"] as $key => $FILE) {
                        ?>
                        <div class="col-md-<?= ($arItem['PROPERTIES']['BANNER_TYPE']['VALUE'] == '1' ? '12' : '4') ?> col-xs-12">
                            <? $FILE_SRC = CFile::GetFileArray($FILE)["SRC"]; ?>
                            <div>
                                <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'][$key] ?>">
                                    <img style=" max-height: 100%; " class="img-responsive"
                                         src="<?= ($FILE_SRC ? $FILE_SRC : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNkYAAAAAYAAjCB0C8AAAAASUVORK5CYII=') ?>"
                                         alt="<?= $arItem['PROPERTIES']['LINK'][$key]['DESCRIPTION'] ?>">
                                </a>
                            </div>
                            <div class="text-center" style=" margin-top: 20px; ">
								<h5 style="color:#000000"><a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'][$key] ?>"><?= $arItem['PROPERTIES']['LINK']['DESCRIPTION'][$key] ?></a></h5>
                            </div>
                            <div style=" margin-bottom: 2rem; " class="text-center">
                                <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'][$key] ?>" class=""
                                   style=" background-color: transparent; color: #000c20; padding: 0px 1px; border-bottom: 1px solid #ccc; ">Купить</a>
                            </div>
                        </div>
                        <? unset($FILE_SRC);
                        if ($key >= $count) break; ?>
                    <? }
                } ?>    </div>
        </div>
    <? endforeach; ?>
<? endif; ?>