<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<? $this->setFrameMode( true ); ?>
<?use \Bitrix\Main\Localization\Loc;?>
<?if($arResult["SECTIONS"]){?>
	<?global $arTheme;
	$bSlick = ($arParams['NO_MARGIN'] == 'Y');
	$bIcons = ($arParams['SHOW_ICONS'] == 'Y');?>

	<div class="section-compact-list">
		<div class="row<?=($bSlick ? ' margin0' : '');?> flexbox">
<div class='link_all'>
	<a href='<?=$arResult['SECTION']['SECTION_PAGE_URL']?>'>Показать все</a>
</div>
			 <?
			$i = 0;
			$len = count($arResult["SECTIONS"]);
			foreach( $arResult["SECTIONS"] as $arItems ){ 
				if($i < 3){
					$this->AddEditAction($arItems['ID'], $arItems['EDIT_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "SECTION_EDIT"));
					$this->AddDeleteAction($arItems['ID'], $arItems['DELETE_LINK'], CIBlock::GetArrayByID($arItems["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_SECTION_DELETE_CONFIRM')));
					?>
					<div class='link_subsection'>
						<a href="<?=$arItems["SECTION_PAGE_URL"]?>"><span><?=$arItems["NAME"]?></span></a>
					</div>
				<?
				}
				$i++;
			}?>
		</div>
		</div>
<?}?>
